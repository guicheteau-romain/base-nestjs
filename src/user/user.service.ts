import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto, UpdateUserDto } from './dto';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async findOne(id: number): Promise<User | null> {
    return this.userRepository.findOneBy({id:id});
  }
  async findOneByMail(mail: string): Promise<User | null> {
    return this.userRepository.findOneBy({email:mail});
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    const newUser = new User();
    newUser.name = createUserDto.name;
    newUser.email = createUserDto.email;
    newUser.age = createUserDto.age;

    return this.userRepository.save(newUser);
  }

  async update(id: number, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.userRepository.findOneBy({id:id});
    user.name = updateUserDto.name;
    user.email = updateUserDto.email;
    user.age = updateUserDto.age;

    return this.userRepository.save(user);
  }

  async remove(id: number): Promise<boolean> {
    const user = await this.userRepository.findOneBy({id:id});
    const result = await this.userRepository.delete(user.id);

    return result.affected > 0;
  }
}
