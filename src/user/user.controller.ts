import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiResponse } from '@nestjs/swagger';

@ApiTags('user')
@Controller('user')
export class UserController {
    
    @Get("/")
    @ApiResponse({})	
    getHello(): string {
      return "hello";
    }
}
