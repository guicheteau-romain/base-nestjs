import { Injectable } from '@nestjs/common';
import { LoginDTO } from './dto';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {
  constructor(private usersService: UserService) {}

  async validateUser(loginDto : LoginDTO): Promise<any> {
    const user = await this.usersService.findOneByMail(loginDto.email);
    if (user && user.password === loginDto.password) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }
}