import { Controller,  Post, UseGuards, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { LoginDTO } from './dto';
import { AuthService } from './auth.service';


@ApiTags('auth')
@Controller('auth')
export class AuthController {

    constructor(
        private readonly authService: AuthService
      ) {}

    @Post('login')
    @ApiBody({
        type:LoginDTO
    })
    async login(@Body() loginDto : LoginDTO) {
        this.authService.validateUser(loginDto)
        return "ok";
    }
}
