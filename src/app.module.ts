import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { DataSourceModule } from './data-source.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    UserModule,  
    DataSourceModule, AuthModule  
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
